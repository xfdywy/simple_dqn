# Solution of Open AI gym environment "Cartpole-v0" (https://gym.openai.com/envs/CartPole-v0) using DQN and Pytorch.
# It is is slightly modified version of Pytorch DQN tutorial from
# http://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html.
# The main difference is that it does not take rendered screen as input but it simply uses observation values from the \
# environment.
import torch
import gym
import os

gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import copy

import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Manager, Lock

from Model import Network_single, make_model
from Buffer import ReplayMemory_single
from Arguments import get_args
from Agent import agent_single
from EnvWrapper import SimpleDQNWrapper

# import argparse


#
# parser = argparse.ArgumentParser(description='main')
# parser.add_argument('-t' , '--task' , type=str,default = 'CartPole-v0')
# parser.add_argument('-s' , '--suff' , type=int,default = 0)
# parser.add_argument('-id' , '--figure_id' , type=int,default = 0)
#
#
# args = parser.parse_args()
# hyper parameters


def cal_win_agent(list , win_agent_info , args):
    beta = args.beta_cwa
    ret = [x[0] - beta *x[1] for x in list]
    max_ret = np.max(ret)

    # res = np.random.choice(np.flatnonzero((ret == max_ret)))
    # print(res, win_agent_info[0], win_agent_info[1], list[res][0], list[res][1], '$$$$$$$$$$$$')
    # return [res, list[res][0], list[res][1]]

    if max_ret < win_agent_info[0] - beta *win_agent_info[1]  and win_agent_info[3] < args.wait_time_cwa :
        win_agent_info[3] += 1
        # print(-1 ,ret , win_agent_info[0] - beta *win_agent_info[1], '$$$$$$$$$$$$$$$$')
        # print( win_agent_info[0], win_agent_info[1], list[0][0], list[0][1], '!!!!!!!!!!!!!!')
        return [-1, win_agent_info[0] , win_agent_info[1] ]#win_agent_info[2]
    else:
        win_agent_info[3] = 0
        res = np.random.choice(np.flatnonzero((ret == max_ret)))
        # print(res ,  win_agent_info[0] , win_agent_info[1], list[res][0] , list[res][1],'$$$$$$$$$$$$')
        return [res, list[res][0] , list[res][1]]


def run_episodes(args, share_model, share_model_info, flag, idx):
    env = gym.make(args.env_name)
    env = SimpleDQNWrapper(env, args.env_name)
    model = make_model(args, env.state_dim, env.action_dim , idx)
    target_model = make_model(args, env.state_dim, env.action_dim ,idx)
    memory = ReplayMemory_single(args.memory_size)
    agent = agent_single(args.env_name, model, target_model, memory, args, idx)
    this_episode = 0
    while True:

        if agent.episodes > args.episodes: #  or all([x[6] for x in flag] ):
            flag[idx][6] = True
            #print(idx , agent.episodes ,  [x[6] for x in flag], '#########################')
        if all(x[6] for x in flag):
            # print(idx , agent.episodes ,  [x[6] for x in flag], '#########################')
            break

        if agent.episodes > this_episode:
            this_episode = agent.episodes
            # episode done
        agent.step_ahead()
        agent.learn()
        flag[idx][1] = [ agent.total_step, agent.episodes]


        # if agent.total_step % agent.args.update_target_model_freq ==0  and agent.episodes < agent.args.warm_eps:
        #     agent.update_target_model()

        if agent.total_step % args.eval_target_model_freq == 0 :
            # print(agent.total_step, '!!!!!!!!!!!', args.eval_target_model_freq)

            step, ret = agent.evaluate()
            mean_ret, std_ret = np.mean(ret), np.std(ret)

            # this_flag = flag[idx]
            flag[idx][0] = [mean_ret, std_ret]
            flag[idx][3] = True
            # flag[idx] = this_flag
            # flag[id][2] = True

            while True:
                if all([x[3] for x in flag]):
                    if idx == 0:
                        # print(idx , flag[0][1] , '!!!!!!!!!!!!!!!')
                        win_agent,mean,std = cal_win_agent([x[0] for x in flag] ,share_model_info  , args)
                        share_model_info[0] = mean
                        share_model_info[1] = std
                        share_model_info[2] = win_agent
                        # print(share_model_info[3] , '@@@@@@@@@@@@@@')
                        for ii in range(len(flag)):
                            flag[ii][5] = True

                    if flag[idx][5]:
                        win_agent = share_model_info[2]

                        # print( idx, share_model_info[0], share_model_info[1],  share_model_info[2], '$$$$$$$$$$$$')
                        if win_agent == -1:
                            if idx == 0:
                                for ii in range(len(flag)):
                                    flag[ii][2] = True

                        if idx == win_agent:
                            agent.push_share_model(share_model)

                            for ii in range(len(flag)):
                                flag[ii][2] = True

                    if flag[idx][2] :

                        if agent.total_step > args.warm_steps :
                            # print(idx, agent.total_step, '@@@@@@@@@@@@@@@')

                            agent.pull_share_model(share_model)
                        else:
                            agent.update_target_model()

                        flag[idx][4] = True
                if flag[idx][4]:
                    flag[idx][0] = [0, 0]
                    flag[idx][2] = False
                    flag[idx][3] = False
                    flag[idx][4] = False
                    flag[idx][5] = False

                    break


def log_func(args, test_model, this_episode ,this_step , lock, test_process):



    def select_action_greedy( args , state):
        state = torch.FloatTensor(state)
        # if args.use_cuda:
            # state = state.to(args.cuda[-1])
        # print([x.is_cuda for x in share_model.parameters()] , '!!!!!!!!!!!!!!!!!!!!!' , state.is_cuda)
        with torch.no_grad():
            return test_model(state).data.argmax().item()

    def evaluate(args):
        env = gym.make(args.env_name)
        env = SimpleDQNWrapper(env, args.env_name)
        temp_ret = [0] * args.eval_episodes
        temp_step = [0] * args.eval_episodes
        for ii in range(args.eval_episodes):
            nn = 0
            state = env.reset()
            while True:
                nn += 1
                with torch.no_grad():
                    action = select_action_greedy(args , state)
                state, reward, done, info = env.step(action)
                temp_ret[ii] += reward
                if done:
                    temp_step[ii] = nn
                    break
        return ([temp_step, temp_ret])
    for ii in range(args.test_episodes):
        step, ret = evaluate(args)
    log_content = "episode {}, total_step {}, mean_step {:5.2f}, std_step {:5.2f}, mean_ret {:5.2f}, std_ret {:5.2f}".format(
        this_episode, this_step, np.mean(step), np.std(step), np.mean(ret), np.std(ret))
    print( log_content)
    lock.acquire()
    try:
        fp = open(args.log_file_name , 'a+')
        fp.write(log_content)
        fp.write('\n')
        fp.close()
    finally:
        test_process[0] = this_step
        test_process[1] = this_episode
        lock.release()

def get_file_name(args):
    return(args.log_dir +   '_'.join([args.env_name , str(args.agent_number) , str(args.seed) , args.suff]) + '.txt')

def print_inventory(dct):
    print("Items held:")
    for item, amount in dct.items():
        print("{} ({})".format(item, amount))

if __name__ == '__main__':
    torch.multiprocessing.set_start_method("spawn")

    args = get_args()
    args.seed = int(args.suff)



    # env_name = 'CartPole-v0'
    env_name = args.env_name
    # env_name = 'MountainCar-v0'
    # env_name = 'LunarLander-v2'
    # env_name = 'Pendulum-v0'
    figure_id = args.figure_id
    suff = args.suff
    # utm  = 50
    # if gpu is to be used
    # args.use_cuda = args.cuda > -1
    if args.use_cuda:
        assert len(args.cuda) == args.agent_number + 1
        args.cuda =[int(x) for x  in args.cuda  ]

    #
    # FloatTensor = torch.cuda.FloatTensor if args.use_cuda else torch.FloatTensor
    # LongTensor = torch.cuda.LongTensor if args.use_cuda else torch.LongTensor
    # ByteTensor = torch.cuda.ByteTensor if args.use_cuda else torch.ByteTensor
    # Tensor = FloatTensor

    env = gym.make(env_name)
    env = SimpleDQNWrapper(env, env_name)

    share_model = make_model(args, env.state_dim, env.action_dim,-1)


    share_model.share_memory()
    log_file_name = get_file_name(args)
    args.log_file_name = log_file_name
    print(log_file_name)
    print_inventory(args.__dict__ )
    with open(log_file_name , 'w+') as fp:
        fp.write(log_file_name)
        fp.write('\n \n')
        fp.write('{}'.format(args.__dict__ ))
        fp.write('\n \n')


    with Manager() as manager:
        ###[            0,              1,                          2,          3,          4,          5,              6   ]
        ###[            [mean , std],   [total_step , episodes],    push done,  eval done,  pull done, cal max done,    game done   ]
        flag = manager.list([manager.list([[-2, -2], [0, 0],         False,     False,      False,      False,          False]) for x in range(args.agent_number)])
        ####                            [mean,   std ,      idx,    wait time]
        share_model_info = manager.list([-10**8 , -10**8 , 0,       0])
        test_process = manager.list([0,0])
        lock = Lock()


        all_process = []
        for ii in range(args.agent_number):
            process = Process(target=run_episodes, args=(args, share_model , share_model_info , flag, ii))
            process.start()
            all_process.append(process)

        this_episode = 0# flag[0][1][1]
        this_step = 0 # flag[0][1][0]
        while True:
            
            if flag[0][1][1] % args.log_interval == 0 and this_episode < flag[0][1][1]:
          
            #if flag[0][1][0] % args.log_interval == 0 and this_step < flag[0][1][0]:
                this_episode = flag[0][1][1]
                this_step = flag[0][1][0]
                # print(this_episode,'!!!!!!!!')
                test_model = copy.deepcopy(share_model)
                test_model = test_model.to('cpu')
                process = Process(target=log_func, args=(args,test_model , this_episode ,this_step , lock ,test_process ))
                process.start()



            if this_episode > args.episodes:
                while True:
                    if test_process[1] > args.episodes:
                        #print(test_process[1] , '&&&&&&&&&&&&&&&&&&&')
                        break
                print('saving final model')
                torch.save(test_model, os.path.join(args.save_dir, '_'.join([args.env_name , str(args.agent_number) , str(args.seed) , args.suff])  + ".pt"))
                print('save model done!')

                break


        # all_process.append(process)
        for p in all_process:
            p.join()

