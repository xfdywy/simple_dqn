import os

os.mkdir('simple_dqn_per_step_bash')
os.chdir('simple_dqn_per_step_bash')


# n = 0
# gpu = [0,0,1,2,3]
 
n = 0
# code_root = '/hdfs/pnrsy/v-yuewng/project/simple_dqn'
# log_root = '/hdfs/pnrsy/v-yuewng/log/log_learning_from_peer/'
# shelllog_root = '/hdfs/pnrsy/v-yuewng/shelllog/shelllog_learning_from_peer/'

# code_root = '/home/yuewang/project/simple_dqn'
# log_root = '/home/yuewang/log/log_learning_from_peer/'
# shelllog_root = '/home/yuewang/shelllog/shelllog_learning_from_peer/'

# code_root = '/root/project/simple_dqn'
# log_root = '/root/log/log_learning_from_peer/'
# shelllog_root = '/root/shelllog/shelllog_learning_from_peer/'
#
# hdfs_log_root ='/workspace/hdfs/v-yuewng/log/log_learning_from_peer/'
# hdfs_shelllog_root = '/workspace/hdfs/v-yuewng/shelllog/shelllog_learning_from_peer/'


code_root = '/hdfs/pnrsy/v-yuewng/project/simple_dqn'
log_root = '/workspace/log/log_learning_from_peer_per_step/'
shelllog_root = '/workspace/shelllog/shelllog_learning_from_peer_per_step/'
save_dir = '/workspace/save_dir/save_dir_learning_from_peer_per_step/'



hdfs_log_root = '/hdfs/pnrsy/v-yuewng/log/log_learning_from_peer_per_step/'
hdfs_shelllog_root = '/hdfs/pnrsy/v-yuewng/shelllog/shelllog_learning_from_peer_per_step/'
hdfs_save_dir =   '/hdfs/pnrsy/v-yuewng/save_dir/save_dir_learning_from_peer_per_step/'


# log_root = '/hdfs/pnrsy/v-yuewng/log/log_learning_from_peer/'
# shelllog_root = '/hdfs/pnrsy/v-yuewng/shelllog/shelllog_learning_from_peer/'


# hdfs_log_root ='/workspace/hdfs/v-yuewng/log/log_learning_from_peer/'
# hdfs_shelllog_root = '/workspace/hdfs/v-yuewng/shelllog/shelllog_learning_from_peer/'



mkdir = 'mkdir -p  %s && mkdir -p %s  && mkdir -p %s '%(log_root , shelllog_root , save_dir)


for jj in range(6):
    for nii, ii in enumerate([1, 2, 4, 8,   ]):
        with open('%d_multi_cartpole_%d_%d.sh' % (n, ii, jj), 'w') as fp:
            bash = []

            bash.append(mkdir)
            bash.append('cd %s' % (code_root))


            n+=1
            wtc = 10
            ws = 700
            if ii ==1:
                wtc=1
                ws = 45000

            bash.append('python simple_dqn_learning_from_peer_per_step.py -ed 100 -ts 45000 -b 32 -m 10000 -utm 50 -etm 50 -ga 0.99 -an %d -ee 5 -lr 0.001 -h1 64 -h2 64 -ws %d -bc 0.5 -wtc %d  -e CartPole-v0 --log-dir %s  --save-dir %s  -li 50  -s %d   | tee %s%d_multi_cartpole_%d_%d.txt '%(
                                                                                                                                        ii,                                 ws,             wtc ,                   log_root ,      save_dir,       jj,       shelllog_root, n,       ii,jj))

            bash.append('cp  %s*  %s  && cp %s* %s  &&   cp %s* %s'%(log_root , hdfs_log_root , shelllog_root, hdfs_shelllog_root , save_dir , hdfs_save_dir) )

            fp.write('\n'.join(bash))

#
for jj in range(6):
    for nii, ii in enumerate([1, 2, 4, 8,   ]):
        with open('%d_multi_MountainCar_%d_%d.sh' % (n, ii, jj), 'w') as fp:
            bash = []
            bash.append(mkdir)
            bash.append('cd %s' %(code_root))

            n+=1
            wtc = 5
            ws = 30000
            if ii ==1:
                wtc=1
                ws = 350000

            bash.append('python simple_dqn_learning_from_peer_per_step.py -ed 5000 -ts 350000 -b 32 -m 20000 -utm 200 -etm 200 -ga 0.99 -an %d -ee 3 -lr 0.001 -h1 64 -h2 64 -ws %d -bc 0.5 -wtc %d  -e MountainCar-v0  --log-dir %s  --save-dir %s     -li 200     -s %d   | tee %s%d_multi_mountaincar_%d_%d.txt '%(
                                                                                                                                             ii,                                 ws,             wtc ,                   log_root ,      save_dir,                  jj,       shelllog_root, n,       ii,jj))

            # bash.append('python simple_dqn_learning_from_peer_per_step.py -ed 5000 -ep 2000 -b 32 -m 20000 -utm 200 -etm 200 -ga 0.99 -an %d -ee 3 -lr 0.001 -h1 64 -h2 64 -we 300 -bc 0.2 -wtc   %d  -e MountainCar-v0 --log-dir %s  --save-dir %s -s %d | tee   %s%d_multi_MountainCar_%d_%d.txt   '%(ii , wtc , log_root, save_dir , jj , shelllog_root , n , ii,jj))
            bash.append('cp  %s*  %s  && cp %s* %s  &&   cp %s* %s' % (
            log_root, hdfs_log_root, shelllog_root, hdfs_shelllog_root, save_dir, hdfs_save_dir))
            fp.write('\n'.join(bash))



for jj in range(6):
    for nii, ii in enumerate([1, 2, 3, 4 ]):
        with open('%d_multi_LunarLander_%d_%d.sh' % (n, ii, jj), 'w') as fp:
            bash = []
            bash.append(mkdir)
            bash.append('cd %s' %(code_root))

            n+=1
            wtc = 10
            ws = 150000
            if ii ==1:
                wtc=1
                ws = 750000
            if ii ==1:
                cuda = ' 0 1 '
            elif ii ==2 :
                cuda = ' 0  1 2 '
            elif ii ==3:
                cuda = ' 0 1 2 3 '
            elif ii ==4:
                cuda = ' 0 1 2 3 0 '

            bash.append('python simple_dqn_learning_from_peer_per_step.py -ed 100 -ts 750000 -b 32 -m 10000 -utm 300 -etm 300 -ga 0.99 -an %d -ee 3 -lr 0.0001 -h1 512 -h2 256 -ws %d -bc 0.5 -wtc %d  -e LunarLander-v2  --cuda %s  --use_cuda --log-dir %s  --save-dir %s  -li 300  -s %d   | tee %s%d_multi_lunarlander_%d_%d.txt '%(
                                                                                                                                        ii,                                      ws,             wtc ,                         cuda,                  log_root ,      save_dir,              jj,       shelllog_root, n,       ii,jj))
            # bash.append('python simple_dqn_learning_from_peer_per_step.py -ed 10000 -ep 2500 -b 64 -m 20000 -utm 300 -etm 300 -ga 0.99 -an %d -ee 3 -lr 0.0001 -h1 512 -h2 256 -we 400 -bc 0.5 -wtc %d -e LunarLander-v2    --cuda %s  --use_cuda --log-dir %s  --save-dir %s -s %d   |tee %s%d_multi_LunarLander_%d_%d.txt   '%(ii , wtc , cuda , log_root, save_dir , jj , shelllog_root, n,ii,jj))
            bash.append('cp  %s*  %s  && cp %s* %s  &&   cp %s* %s' % (
            log_root, hdfs_log_root, shelllog_root, hdfs_shelllog_root, save_dir, hdfs_save_dir))
            fp.write('\n'.join(bash))
#


# for jj in range(10):
#     with open('%d_multi_lunarlander_%d.sh' % (n,  jj), 'w') as fp:
#         bash = []
#         bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#         for nii,ii in enumerate([1, 4, 8, 12, 16]):
#             n+=1
#             bash.append('CUDA_VISIBLE_DEVICES=%d python simple_dqn_multi_new.py -t LunarLander-v2 -s %d -an %d -ef 3000 -el 5 -te 4000 -utm 15000 &'%(gpu[nii],jj , ii))
#         fp.write('\n'.join(bash))
#
#
#
#

# for jj in range(10):
#     n+=1
#     with open('%d_single_mountaincar_1_%d.sh'%(n,jj) , 'wb') as fp:
#         bash =[]
#         bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#         bash.append('python simple_dqn_mountaincar.py -s %d '%(jj))
#         fp.write('\n'.join(bash))
#
#
# for ii in [1,4,6,10,16]:
#     for jj in range(10):
#         n+=1
#         with open('%d_multi_mountaincar_%d_%d.sh'%(n,ii,jj) , 'wb') as fp:
#             bash =[]
#             bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#             bash.append('python simple_dqn_mountaincar_multi.py -s %d -an %d'%(jj , ii))
#             fp.write('\n'.join(bash))
#
#
# for jj in range(10):
#     n+=1
#     with open('%d_single_lunarlander_1_%d.sh'%(n,jj) , 'wb') as fp:
#         bash =[]
#         bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#         bash.append('python simple_dqn_lunarlander.py -s %d '%(jj))
#         fp.write('\n'.join(bash))
#
#
# for ii in [1,4,6,10,16]:
#     for jj in range(10):
#         n+=1
#         with open('%d_multi_lunarlander_%d_%d.sh'%(n,ii,jj) , 'wb') as fp:
#             bash =[]
#             bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#             bash.append('python simple_dqn_lunarlander_multi.py -s %d -an %d'%(jj , ii))
#             fp.write('\n'.join(bash))