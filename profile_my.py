import torch
import gym
gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import copy
import time

import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Manager, Lock

from Model import Network_single, make_model
from Buffer import ReplayMemory_single
from Arguments import get_args
from Agent import agent_single
from EnvWrapper import SimpleDQNWrapper



def run_episodes(args, share_model, share_model_info, flag, idx):
    env = gym.make(args.env_name)
    env = SimpleDQNWrapper(env, args.env_name)
    model = make_model(args, env.state_dim, env.action_dim , idx)
    target_model = make_model(args, env.state_dim, env.action_dim ,idx)
    memory = ReplayMemory_single(args.memory_size)
    agent = agent_single(args.env_name, model, target_model, memory, args, idx)
    this_episode = 0
    this_step = 0
    this_time = 0
    this_time1 = 0
    this_time2 =  0
    while True:


        if agent.episodes > args.episodes:
            break

        if agent.episodes > this_episode:
            this_episode = agent.episodes
            # print(this_episode, '\t' , (agent.total_step - this_step)  / this_time1, '\t' , (agent.total_step - this_step)  / this_time2 )
            this_time1 = 0
            this_time2 = 0
            this_step = agent.total_step
            # episode done
        this_time_tmp1 = time.time()
        agent.step_ahead()
        this_time_tmp2 = time.time()
        agent.learn()
        this_time_tmp3 = time.time()
        this_time1 += this_time_tmp2 - this_time_tmp1
        this_time2 += this_time_tmp3 - this_time_tmp2


args = get_args()
args.seed = int(args.suff)

figure_id = args.figure_id
suff = args.suff
env_name = args.env_name
env = gym.make(env_name)
env = SimpleDQNWrapper(env, env_name)

share_model = make_model(args, env.state_dim, env.action_dim, -1)

share_model.share_memory()
# log_file_name = get_file_name(args)
# args.log_file_name = log_file_name
# print(log_file_name)
# print_inventory(args.__dict__)

run_episodes(args, 0 , 0 , 0, 0)