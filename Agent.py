import torch
import gym
gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import copy

from EnvWrapper import SimpleDQNWrapper



class agent_single:
    def __init__(self , env_name , model , target_model , memory ,args , idx):
        self.env_name = env_name
        self.env = self.make_env()
        self.model = model
        self.memory = memory
        self.args = args
        self.total_step = 0
        self.episodes =0

        self.idx = idx
        self.target_model = target_model
        self.optimizer  = optim.Adam(model.parameters(), args.lr)
        self.Loss = torch.nn.MSELoss()
        self.update_target_model()
        self.args.eps_decay = self.cal_my_ed()
        # self.FloatTensor = torch.cuda.FloatTensor if args.use_cuda else torch.FloatTensor
        # self.LongTensor = torch.cuda.LongTensor if args.use_cuda else torch.LongTensor
        # ByteTensor = torch.cuda.ByteTensor if args.use_cuda else torch.ByteTensor
        # Tensor = FloatTensor

        self.state = self.env.reset()

    def select_action(self , state):

        args = self.args
        state = torch.FloatTensor(state)



        if args.use_cuda:
            state = state.to(args.cuda[self.idx])
        sample = random.random()


        eps_threshold = args.eps_end + (args.eps_start - args.eps_end) * math.exp(-1. * self.total_step / args.eps_decay)
        # self.total_step += 1
        if sample > eps_threshold:
            with torch.no_grad():
                return self.model(state).cpu().data.argmax().item()
        else:
            return  random.randrange(self.env.action_dim)

    def select_action_greedy(self , state):

        args = self.args
        state = torch.FloatTensor(state)

        if args.use_cuda:
            state = state.to(args.cuda[self.idx])

        with torch.no_grad():
            return self.model(state).cpu().data.argmax().item()


    def cal_my_ed(self):
        if self.idx == 0 :
            return(self.args.eps_decay)
        else:
            tmp = np.linspace(-3 , 3 , self.args.agent_number + 1)

            return(self.args.eps_decay / np.exp(tmp[self.idx]) )




    def evaluate(self):
        # print(self.idx, 'evaluate')
        args = self.args
        env = self.make_env()
        temp_ret = [0] * args.eval_episodes
        temp_step = [0] * args.eval_episodes
        for ii in range(args.eval_episodes):
            nn = 0
            state = env.reset()
            while True:
                nn += 1
                with torch.no_grad():
                    action =  self.select_action(state)
                state, reward, done, info = env.step(action)
                temp_ret[ii] += reward
                if done:
                    temp_step[ii] = nn
                    break
        return ([temp_step, temp_ret])

    def sync_data(self):
        pass

    def make_env(self):
        env = gym.make(self.env_name)
        env = SimpleDQNWrapper( env, self.env_name)
        return(env)

    def update_target_model(self):
        self.target_model.load_state_dict(copy.deepcopy(self.model.state_dict()))

    def learn(self, ):
        args = self.args

        #
        # if self.total_step %  args.update_target_model_freq == 0:
        #     self.update_target_model( )

        if len(self.memory) < args.batch_size:
            return

        # random transition batch is taken from experience replay memory
        transitions = self.memory.sample(args.batch_size)
        batch_state, batch_action, batch_next_state, batch_reward, batch_done = zip(*transitions)
        if args.use_cuda:
            batch_state = torch.cat(batch_state).to(args.cuda[self.idx])
            batch_action = torch.cat(batch_action).to(args.cuda[self.idx])
            batch_reward = torch.cat(batch_reward).to(args.cuda[self.idx])
            batch_next_state = torch.cat(batch_next_state).to(args.cuda[self.idx])
            batch_done = torch.cat(batch_done).to(args.cuda[self.idx])
        else:
            batch_state = torch.cat(batch_state)
            batch_action = torch.cat(batch_action)
            batch_reward = torch.cat(batch_reward)
            batch_next_state = torch.cat(batch_next_state)
            batch_done = torch.cat(batch_done)
        batch_action = batch_action.view(-1 ,1)
        # print([x.is_cuda for x in self.model.parameters()],'!!!!!!!!!!!!!!!!!!!!')
        # print(batch_action.shape ,self.model(batch_state).shape )
        # current Q values are estimated by NN for all actions
        current_q_values = self.model(batch_state).gather(1, batch_action)
        # expected Q values are estimated from actions which gives maximum Q value
        max_next_q_values = self.target_model(batch_next_state).detach().max(1)[0]
        # max_next_q_values = self.get_max_next_q_values()
        expected_q_values = batch_reward + (args.gamma * max_next_q_values) * (1 - batch_done)
        expected_q_values = expected_q_values.view(-1, 1)

        # loss is measured from error between current and newly expected Q values
        # loss = F.smooth_l1_loss(current_q_values, expected_q_values)

        loss = self.Loss(current_q_values, expected_q_values)

        # backpropagation of loss to NN
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def pull_share_model(self,share_model):
        self.target_model.load_state_dict(copy.deepcopy(share_model.state_dict()))

    def push_share_model(self,share_model):
        share_model.load_state_dict(copy.deepcopy(self.model.state_dict()))

    def step_ahead(self):



        state = self.state
        action = self.select_action([ state])
        next_state, reward, done, _ = self.env.step(action)
        done = 1 if done else 0


        to_push = (torch.FloatTensor([state]),
                   torch.LongTensor([action]),
                     torch.FloatTensor([next_state]),
                     torch.FloatTensor([reward]),
                     torch.FloatTensor([done]))

        self.memory.push( to_push)

        self.state = next_state
        self.total_step +=1

        # if self.total_step % self.args.update_target_model_freq ==0  and self.episodes < self.args.warm_eps:
            # self.update_target_model()


        if done:
            self.state = self.env.reset()
            self.episodes +=1




    def get_max_next_q_values(self):
        pass



