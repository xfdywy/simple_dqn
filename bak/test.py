import torch
import gym
gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import copy
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Manager, Array, Queue
import time
import argparse
import time


class ReplayMemory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []

    def push(self, transition):
        self.memory.append(transition)
        if len(self.memory) > self.capacity:
            del self.memory[0]

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class Network(nn.Module):
    def __init__(self , state_dim , action_dim):
        nn.Module.__init__(self)
        self.l1 = nn.Linear(state_dim, 10)
        self.l2 = nn.Linear(10 , 10)
        self.l3 = nn.Linear(10, action_dim)

    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = self.l3(x)
        return x


def a(id):
    print(id , '~~~~~')

def cal_win_agent(list):
    ret = [x[0] for x in list]
    # print(ret)
    res = np.random.choice(np.flatnonzero((ret == np.max(ret))))
    # res = np.argmax(ret)
    return res



def train_test(flag, share,idx):
    flag[0][0] = [1,1]
    # print(flag[0][0],'#######'  , len(flag))
#



# def train(flag, share,idx):
#     mean_ret = np.random.random(1)
#     std_ret = 1
#     time.sleep(idx / 10)
#     print(idx , len(flag) , '############')
#
#     this_flag = flag[idx]
#
#     this_flag[0] = [mean_ret, std_ret]
#     this_flag[3] = True
#
#     flag[idx] = this_flag
#     while True:
#         if all([x[3] for x in flag]):
#
#             win_agent = cal_win_agent([x[0] for x in flag])
#             # print('2', idx, flag[idx] , win_agent)
#             if idx == win_agent:
#
#                 share[0]  = idx
#
#                 for ii in range(len(flag)):
#                     this_flag =  flag[ii]
#                     this_flag[2] = True
#                     flag[ii] = this_flag
#             # print('3', idx, flag[idx], win_agent, share)
#
#             if flag[idx][2]:
#                 this_flag = flag[idx]
#                 this_flag[5] = share[0]
#                 this_flag[4] = True
#                 flag[idx] = this_flag
#                 print('3', idx, flag[idx], win_agent, share)
#                 break
#         if flag[idx][4]:
#             # flag[id] = [[0, 0], False, False, False, False]
#             break


def train(flag, share,idx):
    # np.random.seed(idx)
    mean_ret = np.random.random(1)
    # print(mean_ret, '%%%%%%%%%')
    std_ret = -3
    time.sleep(idx / 100)
    # print(idx , len(flag) , '############')



    flag[idx][0] = [mean_ret, std_ret]
    flag[idx][3] = True
    # print([x[0] for x in flag]  , '%%%%%%%%')


    while True:
        if all([x[3] for x in flag]):

            win_agent = cal_win_agent([x[0] for x in flag])

            # print('2', idx, flag[idx] , win_agent)
            if idx == win_agent:

                share[0]  = win_agent

                for ii in range(len(flag)):

                    flag[ii][2] = True

            # print('3', idx, flag[idx], win_agent, share)

            if flag[idx][2]:

                flag[idx][5] = share[0]
                flag[idx][4] = True
                # time.sleep(1)

                print('3', idx, flag[idx], win_agent, share[0] , share[0] == win_agent)
                break
        if flag[idx][4]:
            # flag[id] = [[0, 0], False, False, False, False]
            break


if __name__ =='__main__':
    parser = argparse.ArgumentParser(description='simple dqn task using learning from peer')

    parser.add_argument('-e', '--env_name', type=str, default='CartPole-v0')
    parser.add_argument('-f', '--f', type=str, default='aaa')

    args = parser.parse_args()
    print(args.env_name)
    for ii,jj in args.__dict__.items():
        print(ii , jj)


    # with Manager() as manager:
    #     flag = manager.list( [manager.list([[-2, -2], False, False, False, False , -1  ]) for x in range(3)]  )
    #     share = manager.list( [-1])
    #     for ii in range(3):
    #         process = Process(target=train  , args=(flag , share ,  ii))
    #         process.start()
    #     process.join()

        # print([list(x) for x in flag], share)