# Solution of Open AI gym environment "Cartpole-v0" (https://gym.openai.com/envs/CartPole-v0) using DQN and Pytorch.
# It is is slightly modified version of Pytorch DQN tutorial from
# http://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html.
# The main difference is that it does not take rendered screen as input but it simply uses observation values from the \
# environment.
import torch
import gym
gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import copy
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Manager
import time
import argparse

parser = argparse.ArgumentParser(description='main')
parser.add_argument('-t' , '--task' , type=str,default = 'MountainCar-v0')
parser.add_argument('-s' , '--suff' , type=int,default = 0)
parser.add_argument('-id' , '--figure_id' , type=int,default = 0)
parser.add_argument('-an' , '--agent_number' , type=int, default=1)


args = parser.parse_args()
# hyper parameters
EPISODES = 3000  # number of episodes
EPS_START = 1  # e-greedy threshold start value
EPS_END = 0.01  # e-greedy threshold end value
EPS_DECAY = 10000  # e-greedy threshold decay
GAMMA = 1  # Q-learning discount factor
LR = 0.001  # NN optimizer learning rate
HIDDEN_LAYER1 = 32  # NN hidden layer size
HIDDEN_LAYER2 = 32  # NN hidden layer size
BATCH_SIZE = 32  # Q-learning batch size
MEMORY_SIZE = 20000 # replay buffer size
# env_name = 'CartPole-v0'
env_name = args.task
# env_name = 'MountainCar-v0'
#env_name = 'LunarLander-v2'
# env_name = 'Pendulum-v0'
figure_id = args.figure_id
suff = args.suff
utm  = 200
# if gpu is to be used
use_cuda = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if use_cuda else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if use_cuda else torch.ByteTensor
Tensor = FloatTensor
agent_num = args.agent_number

logroot = './log/'

class ReplayMemory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []

    def push(self, transition):
        self.memory.append(transition)
        if len(self.memory) > self.capacity:
            del self.memory[0]

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class Network(nn.Module):
    def __init__(self , state_dim , action_dim, agent_num):
        nn.Module.__init__(self)
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.agent_num  = agent_num
        self.l1 = nn.Linear(state_dim, HIDDEN_LAYER1)
        self.l2 = nn.Linear(HIDDEN_LAYER1 , HIDDEN_LAYER2)
        self.multi_agent_layer = nn.ModuleList()
        for ii in range(agent_num):
            self.multi_agent_layer.append(self._make_layer())


    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = x.view(x.size(0),-1)
        # print(x.shape)
        multi_agent = []
        for ii in self.multi_agent_layer:
            multi_agent.append(ii(x))
        out = torch.stack(multi_agent , 2)
        return out

    def _make_layer(self):
        return(nn.Sequential(nn.Linear(HIDDEN_LAYER2 , self.action_dim)))


env = gym.make(env_name)
state_dim = env.observation_space.shape[0]
action_dim = env.action_space.n

assert type(state_dim) is int
assert type(action_dim) is int

model = Network(state_dim , action_dim ,agent_num)

if use_cuda:
    model.cuda()
    # target_model.cuda()


memory = ReplayMemory(MEMORY_SIZE)
optimizer = optim.Adam(model.parameters(), LR)
steps_done = 0
episode_durations = []
episode_rewards = []


def evaluation(model,env_name , win_agent , episode = 5):
    env = gym.make(env_name)
    # state = env.reset()
    temp_ret = [0] * episode
    temp_step = [0] * episode

    for ii in range(episode):
        nn = 0
        state = env.reset()
        while True:
            nn += 1
            with torch.no_grad():
                # print()
                action = model(Variable(FloatTensor([state])).type(FloatTensor))[:, :, win_agent].data.max(1)[1].view(
                    1, 1)

            state, reward, done, info = env.step(action.cpu().data.numpy()[0, 0])

            temp_ret[ii] += reward
            if done:
                temp_step[ii] = nn
                break
    return([temp_step , temp_ret])




class Target_model():
    def __init__(self , env_name):

        self.env = gym.make(env_name)
        self.target_model = Network(state_dim, action_dim, agent_num)
        if use_cuda:
            self.target_model.cuda()
            self.target_model.share_memory()

    def update_target_model(self,model ):
        self.target_model.load_state_dict(copy.deepcopy(model.state_dict()))

    def eval_target_model(self):
        with Manager() as manager:
            ret = manager.list([0]*agent_num)
            record =[]
            for ii in range(agent_num):
                process = Process(target=self.eval_one_target_model,args=(self.env ,ii, ret))
                process.start()
                record.append(process)
            for p in record:
                p.join()

            self.win_agent = np.random.choice(np.flatnonzero((ret == np.max(ret))))
            print('!!!!! win agent is now %d'%(self.win_agent) , ret)


    def eval_one_target_model(self  ,env, agent_id , ret , episode=3):
        temp_ret =[0]*episode

        for ii in range(episode):
            nn=0
            state = env.reset()
            while True:
                nn+=1
                with torch.no_grad():
                    # print()
                    action = self.target_model(Variable(FloatTensor([state])).type(FloatTensor))[:,:,agent_id].data.max(1)[1].view(1, 1)

                state , reward, done , info = env.step(action.cpu().data.numpy()[0,0])

                temp_ret[ii]+=reward
                if done :
                    break
        ret[agent_id] = np.mean(temp_ret)




def select_action(state,win_agent):
    global steps_done
    sample = random.random()
    eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
    steps_done += 1
    if sample > eps_threshold:
        return model(Variable(state).type(FloatTensor))[:,:,win_agent].data.max(1)[1].view(1, 1)
    else:
        return LongTensor([[random.randrange(2)]])


def run_episode(e, environment,fp , target_model):

    if e% 10 ==0:
        res_steps, res_return =  evaluation(model=model , env_name=env_name , win_agent=target_model.win_agent,)
        print('%%%%%%%%%%%%%%%episode{0}, mean {1}, var {2}'.format(e, np.mean(res_return), np.var(res_return)))
        # fp.write(','.join([str(e) , str(np.mean(res_steps)),str(np.var(res_steps)),\
        #                    str(np.mean(res_return)),str(np.var(res_return)) ]))
        fp.write('%d,%.4f,%.4f, %.4f, %.4f'%(e ,np.mean(res_steps) , np.var(res_steps) ,
                                                  np.mean(res_return) , np.var(res_return)))

        fp.write('\n')
        fp.flush()

    state = environment.reset()
    steps = 0
    episode_rewards.append(0)
    win_agent = target_model.win_agent
    while True:

        action = select_action(FloatTensor([state]) , win_agent)
        next_state, reward, done, _ = environment.step(action.cpu().numpy()[0,0])


        # print(done)

        done  = 1 if done else 0
        # negative reward when attempt ends

        if  next_state[0]>=0.5:
            reward = 1

        

        episode_rewards[-1]+=reward

        memory.push((FloatTensor([state]),
                     action,  # action is already a tensor
                     FloatTensor([next_state]),
                     FloatTensor([reward]) ,
                     FloatTensor([done])))

        learn()


        state = next_state
        steps += 1

        if done:
            print("{2} Episode {0} finished after {1} steps with return {3}"
                  .format(e, steps, '\033[92m' if steps >= 195 else '\033[99m' , episode_rewards[-1]))
            # episode_durations.append(steps)
            # plot_durations()
            #plot_rewards()
            # fp.write(','.join([str(e) , str(steps) , str(episode_rewards[-1])]))
            # fp.write('\n')
            break


def learn():
    global steps_done

    if steps_done % utm == 0:
        target_model.update_target_model(model )
        target_model.eval_target_model()


    if len(memory) < BATCH_SIZE:
        return

    # random transition batch is taken from experience replay memory
    transitions = memory.sample(BATCH_SIZE)
    batch_state, batch_action, batch_next_state, batch_reward,batch_done = zip(*transitions)

    batch_state = Variable(torch.cat(batch_state))
    batch_action = Variable(torch.cat(batch_action))
    batch_reward = Variable(torch.cat(batch_reward))
    batch_next_state = Variable(torch.cat(batch_next_state))
    batch_done = Variable(torch.cat(batch_done))

    # current Q values are estimated by NN for all actions
    batch_action_expand = batch_action.unsqueeze(2)
    batch_action_expand = batch_action_expand.expand(batch_action_expand.size(0) , 1, agent_num)
    ##batch_action_expand  64,1,5

    batch_reward_expand = batch_reward.view(-1,1,1)
    batch_reward_expand = batch_reward_expand.expand(batch_reward_expand.size(0), 1, agent_num)
    ##batch_reward_expand 64,1,5

    batch_done_expand = batch_done.view(-1, 1, 1)
    batch_done_expand = batch_done_expand.expand(batch_done_expand.size(0), 1, agent_num)
    ##batch_done_expand 64,1,5




    current_q_values = model(batch_state).gather(1, batch_action_expand)

    # expected Q values are estimated from actions which gives maximum Q value

    tm = target_model.target_model(batch_next_state)[:, :, target_model.win_agent]
    max_next_q_values = tm.detach().max(1)[0]
    max_next_q_values = max_next_q_values.unsqueeze(1)
    max_next_q_values = max_next_q_values.unsqueeze(2)
    max_next_q_values = max_next_q_values.expand(max_next_q_values.size(0) , max_next_q_values.size(1) , agent_num)
    ##max_next_q_values64,1,5

    expected_q_values = batch_reward_expand + (GAMMA * max_next_q_values) *(1- batch_done_expand)
    # expected_q_values = expected_q_values.view(-1,1)

    # loss is measured from error between current and newly expected Q values
    #loss = F.smooth_l1_loss(current_q_values, expected_q_values)
    Loss = torch.nn.MSELoss()
    loss = Loss(current_q_values, expected_q_values)

    # backpropagation of loss to NN
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


if __name__ == "__main__":
    mp.set_start_method('spawn')


    with open(logroot+'multiagent_%s_%s_%s.log'%(agent_num,env_name,suff),'w+') as fp:
        fp.write(','.join(['episode', 'stepsmean', 'stepsvar', 'returnmean', 'returnvar']))
        fp.write('\n')
        target_model = Target_model(env_name)
        target_model.update_target_model(model)
        target_model.eval_target_model()
        # target_model.update_target_model(model )
        for e in range(EPISODES):
            run_episode(e, env,fp,target_model)



        print('Complete')

        env.close()

