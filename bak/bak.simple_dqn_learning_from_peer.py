# Solution of Open AI gym environment "Cartpole-v0" (https://gym.openai.com/envs/CartPole-v0) using DQN and Pytorch.
# It is is slightly modified version of Pytorch DQN tutorial from
# http://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html.
# The main difference is that it does not take rendered screen as input but it simply uses observation values from the \
# environment.
import torch
import gym
gym.logger.setLevel(40)
from gym import wrappers
import random
import math
import numpy as np
import copy



import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Manager




from Model import Network_single, make_model
from Buffer import  ReplayMemory_single
from Arguments import  get_args
from Agent import agent_single
from EnvWrapper import SimpleDQNWrapper

# import argparse





#
# parser = argparse.ArgumentParser(description='main')
# parser.add_argument('-t' , '--task' , type=str,default = 'CartPole-v0')
# parser.add_argument('-s' , '--suff' , type=int,default = 0)
# parser.add_argument('-id' , '--figure_id' , type=int,default = 0)
#
#
# args = parser.parse_args()
# hyper parameters
args = get_args()
args.seed = int(args.suff)




# env_name = 'CartPole-v0'
env_name = args.task
# env_name = 'MountainCar-v0'
#env_name = 'LunarLander-v2'
# env_name = 'Pendulum-v0'
figure_id = args.figure_id
suff = args.suff
# utm  = 50
# if gpu is to be used
args.use_cuda = args.cuda>-1

#
# FloatTensor = torch.cuda.FloatTensor if args.use_cuda else torch.FloatTensor
# LongTensor = torch.cuda.LongTensor if args.use_cuda else torch.LongTensor
# ByteTensor = torch.cuda.ByteTensor if args.use_cuda else torch.ByteTensor
# Tensor = FloatTensor

env = gym.make(env_name)
env = SimpleDQNWrapper(env, env_name)

share_model = make_model(args, env.state_dim , env.action_dim)
if args.use_cuda:
    share_model.to(args.cuda[-1])
share_model.share_memory()


def cal_win_agent(list):
    ret = [x[0] for x in list]
    res = np.random.choice(np.flatnonzero((ret == np.max(ret))))
    return res

def run_episodes(args , share_model, flag , idx):
    env = gym.make(env_name)
    env = SimpleDQNWrapper(env, env_name)
    model = make_model(args, env.state_dim, env.action_dim)
    target_model = make_model(args, env.state_dim, env.action_dim)
    memory = ReplayMemory_single(args.memory_size)
    agent = agent_single(args.env_name, model , target_model , memory , args , flag)
    this_episode = 0
    while True:

        if agent.episodes > args.episodes:
            break

        if agent.episodes > this_episode:
            this_episode = agent.episodes
            # episode done
        agent.step_ahead()
        agent.learn()

        if agent.total_step % args.eval_target_model_freq ==0:
            step,  ret = agent.evaluate()
            mean_ret, std_ret = np.mean(ret) , np.std(ret)

            this_flag = flag[idx]
            this_flag[0] = [mean_ret , std_ret]
            this_flag[3] = True
            flag[idx] = this_flag
            # flag[id][2] = True



            while True:
                if all([x[3] for x in flag]):
                    win_agent = cal_win_agent([x[0] for x in flag])
                    if idx == win_agent:
                        agent.push_share_model(share_model)
                        for ii in range(len(flag)):
                            this_flag = flag[ii]
                            this_flag[2] = True
                            flag[ii] = this_flag
                    if flag[idx][2]:
                        agent.pull_share_model(share_model)
                        this_flag = flag[idx]
                        this_flag[4] = True
                        flag[idx] = this_flag
                if flag[idx][4]:
                    flag[idx]=[[0,0],    False,      False,      False,      False]
                    break






def log_func(share_model , flag):
    pass










if __name__ =='__main__':

    with Manager() as manager:
        ###[            0,              1,                          2,          3,          4   ]
        ###[            [mean , std],   [total_step , episodes],    push done,  eval done,  pull done   ]
        flag = manager.list([manager.list([[-2, -2], [0,0], False, False, False, -1]) for x in range(args.agent_num)])

        all_process =[]
        for ii in range(3):
            process = Process(target=run_episodes, args=(args , share_model , flag  ))
            process.start()
            all_process.append(process)
        process = Process(target=log_func , args=())



        process.join()

        print(flag)



        #
        # def run_episode(e, environment,fp):
        #     if e % 1 == 0:
        #         res_steps , res_return = evaluation(model=model, env_name=env_name, )
        #         print('%%%%%%%%%%%%%%%episode{0}, mean {1}, var {2}'.format(e, np.mean(res_return), np.var(res_return)))
        #         # fp.write(','.join([str(e) , str(np.mean(res_steps)),str(np.var(res_steps)),\
        #         #                    str(np.mean(res_return)),str(np.var(res_return)) ]))
        #         fp.write('%d,%.4f,%.4f, %.4f, %.4f'%(e ,np.mean(res_steps) , np.var(res_steps) ,
        #                                                   np.mean(res_return) , np.var(res_return)))
        #
        #         fp.write('\n')
        #         fp.flush()
        #
        #     state = environment.reset()
        #     steps = 0
        #     episode_rewards.append(0)
        #     while True:
        #
        #         action = select_action(FloatTensor([state]))
        #         next_state, reward, done, _ = environment.step(action.cpu().numpy()[0,0])
        #
        #         if done:
        #             reward = -1
        #
        #         done  = 1 if done else 0
        #         # negative reward when attempt ends
        #
        #
        #         episode_rewards[-1]+=reward
        #
        #         memory.push((FloatTensor([state]),
        #                      action,  # action is already a tensor
        #                      FloatTensor([next_state]),
        #                      FloatTensor([reward]) ,
        #                      FloatTensor([done])))
        #
        #         learn()
        #
        #
        #         state = next_state
        #         steps += 1
        #
        #         if done:
        #             print("{2} Episode {0} finished after {1} steps with return {3}"
        #                   .format(e, steps, '\033[92m' if steps >= 195 else '\033[99m' , episode_rewards[-1]))
        #             # episode_durations.append(steps)
        #             # plot_durations()
        #             #plot_rewards()
        #             # fp.write(','.join([str(e) , str(steps) , str(episode_rewards[-1])]))
        #             # fp.write('\n')
        #             break
        #
        #
        #
        #
        #
        #     with open(logroot + 'singleagent%s_%s.log'%(env_name,suff),'w+') as fp:
        #         fp.write(','.join(['episode' , 'stepsmean' , 'stepsvar' , 'returnmean' , 'returnvar']))
        #         fp.write('\n')
        #         update_target_model(model,target_model)
        #         for e in range(EPISODES):
        #             run_episode(e, env,fp)
        #
        #
        #         print('Complete')
        #
        #         env.close()
        #
