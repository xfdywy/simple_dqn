cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t MountainCar-v0 -s 4 -an 1 -ef 1000 -el 5 -te 2000 -utm 400 &
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t MountainCar-v0 -s 4 -an 4 -ef 1000 -el 5 -te 2000 -utm 400 &
CUDA_VISIBLE_DEVICES=1 python simple_dqn_multi_new.py -t MountainCar-v0 -s 4 -an 8 -ef 1000 -el 5 -te 2000 -utm 400 &
CUDA_VISIBLE_DEVICES=2 python simple_dqn_multi_new.py -t MountainCar-v0 -s 4 -an 12 -ef 1000 -el 5 -te 2000 -utm 400 &
CUDA_VISIBLE_DEVICES=3 python simple_dqn_multi_new.py -t MountainCar-v0 -s 4 -an 16 -ef 1000 -el 5 -te 2000 -utm 400 &