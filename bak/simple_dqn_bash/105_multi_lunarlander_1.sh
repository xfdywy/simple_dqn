cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t LunarLander-v2 -s 1 -an 1 -ef 3000 -el 5 -te 4000 -utm 15000 &
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t LunarLander-v2 -s 1 -an 4 -ef 3000 -el 5 -te 4000 -utm 15000 &
CUDA_VISIBLE_DEVICES=1 python simple_dqn_multi_new.py -t LunarLander-v2 -s 1 -an 8 -ef 3000 -el 5 -te 4000 -utm 15000 &
CUDA_VISIBLE_DEVICES=2 python simple_dqn_multi_new.py -t LunarLander-v2 -s 1 -an 12 -ef 3000 -el 5 -te 4000 -utm 15000 &
CUDA_VISIBLE_DEVICES=3 python simple_dqn_multi_new.py -t LunarLander-v2 -s 1 -an 16 -ef 3000 -el 5 -te 4000 -utm 15000 &