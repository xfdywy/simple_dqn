cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t CartPole-v0 -s 4 -an 1 &
CUDA_VISIBLE_DEVICES=0 python simple_dqn_multi_new.py -t CartPole-v0 -s 4 -an 4 &
CUDA_VISIBLE_DEVICES=1 python simple_dqn_multi_new.py -t CartPole-v0 -s 4 -an 8 &
CUDA_VISIBLE_DEVICES=2 python simple_dqn_multi_new.py -t CartPole-v0 -s 4 -an 12 &
CUDA_VISIBLE_DEVICES=3 python simple_dqn_multi_new.py -t CartPole-v0 -s 4 -an 16 &