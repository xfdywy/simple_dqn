import os

os.mkdir('simple_dqn_bash')
os.chdir('simple_dqn_bash')


n = 0
gpu = [0,0,1,2,3]
 



for jj in range(10):
    with open('%d_multi_cartpole_%d.sh' % (n,  jj), 'w') as fp:
        bash = []
        bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
        for nii,ii in enumerate([1, 4, 8, 12, 16]):
            n+=1
            bash.append('CUDA_VISIBLE_DEVICES=%d python simple_dqn_multi_new.py -t CartPole-v0 -s %d -an %d &'%(gpu[nii] , jj , ii))
        fp.write('\n'.join(bash))



for jj in range(10):
    with open('%d_multi_mountaincar_%d.sh' % (n,  jj), 'w') as fp:
        bash = []
        bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')

        for nii,ii in enumerate([1, 4, 8, 12, 16]):
            n+=1
            bash.append('CUDA_VISIBLE_DEVICES=%d python simple_dqn_multi_new.py -t MountainCar-v0 -s %d -an %d -ef 1000 -el 5 -te 2000 -utm 400 &'%(gpu[nii],jj , ii))
        fp.write('\n'.join(bash))


for jj in range(10):
    with open('%d_multi_lunarlander_%d.sh' % (n,  jj), 'w') as fp:
        bash = []
        bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
        for nii,ii in enumerate([1, 4, 8, 12, 16]):
            n+=1
            bash.append('CUDA_VISIBLE_DEVICES=%d python simple_dqn_multi_new.py -t LunarLander-v2 -s %d -an %d -ef 3000 -el 5 -te 4000 -utm 15000 &'%(gpu[nii],jj , ii))
        fp.write('\n'.join(bash))





# for jj in range(10):
#     n+=1
#     with open('%d_single_mountaincar_1_%d.sh'%(n,jj) , 'wb') as fp:
#         bash =[]
#         bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#         bash.append('python simple_dqn_mountaincar.py -s %d '%(jj))
#         fp.write('\n'.join(bash))
#
#
# for ii in [1,4,6,10,16]:
#     for jj in range(10):
#         n+=1
#         with open('%d_multi_mountaincar_%d_%d.sh'%(n,ii,jj) , 'wb') as fp:
#             bash =[]
#             bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#             bash.append('python simple_dqn_mountaincar_multi.py -s %d -an %d'%(jj , ii))
#             fp.write('\n'.join(bash))
#
#
# for jj in range(10):
#     n+=1
#     with open('%d_single_lunarlander_1_%d.sh'%(n,jj) , 'wb') as fp:
#         bash =[]
#         bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#         bash.append('python simple_dqn_lunarlander.py -s %d '%(jj))
#         fp.write('\n'.join(bash))
#
#
# for ii in [1,4,6,10,16]:
#     for jj in range(10):
#         n+=1
#         with open('%d_multi_lunarlander_%d_%d.sh'%(n,ii,jj) , 'wb') as fp:
#             bash =[]
#             bash.append('cd /var/storage/shared/pnrsy/v-yuewng/project/simple_dqn')
#             bash.append('python simple_dqn_lunarlander_multi.py -s %d -an %d'%(jj , ii))
#             fp.write('\n'.join(bash))