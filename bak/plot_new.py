
import matplotlib
matplotlib.use('Qt5Agg')


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
os.chdir(r'D:\download\log')
all_file = os.listdir('.')

setting = list(set(['_'.join(x.split('_')[:-1]) for x in all_file]))

backgroundc =['lightpink' , 'lightblue','lightgreen' , 'navajowhite','gray' , 'magenta']
linec = ['crimson', 'blue' , 'green', 'orange' , 'black' ,  'darkmagenta' ]

setting = [x  for x in setting if 'CartPole' in x]

# setting = [x  for x in setting if ('multiagent_1_' not in x and 'multiagent_16' not in x) ]
print(setting)

leg =[]

for nii , ii in enumerate(setting):

    if nii ==0:
        continue

    all_file_this_setting = [x for x in all_file if ii in x]
    number = len(all_file_this_setting)
    print(number , all_file_this_setting)

    res = pd.read_csv(all_file_this_setting[0],delimiter=",")

    # 146 cartploe
    n_sample = 300
    # all_mean = np.zeros([number, n_sample])
    all_mean=[]
    leg.append(ii)


    for njj,jj in enumerate(all_file_this_setting):
        res = pd.read_csv(jj,delimiter=",")
        res = res[res['returnmean'].notnull()]
        data  = np.array(res['returnmean'])
        print(len(data))
        if len(data) >= n_sample:

            all_mean.append(data[0:n_sample])
        else :
            continue
            all_mean.append(data[0:n_sample])
            # continue
    all_mean = np.array(all_mean)

    x = range(n_sample)
    y = np.mean(all_mean,0)

    yu = y  +  np.std(all_mean , 0)
    # print(y.shape , yu.shape)
    yd = y  -  np.std(all_mean , 0)

    y  = pd.DataFrame(y).rolling(10).mean().iloc[:,0]


#    if 'multiagent_1' in ii:
#        plt.fill_between(x,yu,y,color=backgroundc[nii] )


    plt.plot(x,y,color=linec[nii])
plt.legend(leg)

plt.show()

