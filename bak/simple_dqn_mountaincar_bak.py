# Solution of Open AI gym environment "Cartpole-v0" (https://gym.openai.com/envs/CartPole-v0) using DQN and Pytorch.
# It is is slightly modified version of Pytorch DQN tutorial from
# http://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html.
# The main difference is that it does not take rendered screen as input but it simply uses observation values from the \
# environment.
import torch
import gym
from gym import wrappers
import random
import math

import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import copy

import argparse

parser = argparse.ArgumentParser(description='main')
parser.add_argument('-t' , '--task' , type=str,default = 'MountainCar-v0')
parser.add_argument('-s' , '--suff' , type=int,default = 0)
parser.add_argument('-id' , '--figure_id' , type=int,default = 0)


args = parser.parse_args()
# hyper parameters
EPISODES = 2000  # number of episodes
EPS_START = 1  # e-greedy threshold start value
EPS_END = 0.01  # e-greedy threshold end value
EPS_DECAY = 10000  # e-greedy threshold decay
GAMMA = 1  # Q-learning discount factor
LR = 0.001  # NN optimizer learning rate
HIDDEN_LAYER1 = 32  # NN hidden layer size
HIDDEN_LAYER2 = 32  # NN hidden layer size
BATCH_SIZE = 32  # Q-learning batch size
MEMORY_SIZE = 20000 # replay buffer size

# env_name = 'CartPole-v0'
env_name = args.task
# env_name = 'MountainCar-v0'
#env_name = 'LunarLander-v2'
# env_name = 'Pendulum-v0'
figure_id = args.figure_id
suff = args.suff
utm  = 200
# if gpu is to be used
use_cuda = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if use_cuda else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if use_cuda else torch.ByteTensor
Tensor = FloatTensor


class ReplayMemory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []

    def push(self, transition):
        self.memory.append(transition)
        if len(self.memory) > self.capacity:
            del self.memory[0]

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class Network(nn.Module):
    def __init__(self , state_dim , action_dim):
        nn.Module.__init__(self)
        self.l1 = nn.Linear(state_dim, HIDDEN_LAYER1)
        self.l2 = nn.Linear(HIDDEN_LAYER1 , HIDDEN_LAYER2)
        self.l3 = nn.Linear(HIDDEN_LAYER2, action_dim)

    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = self.l3(x)
        return x


env = gym.make(env_name)
state_dim = env.observation_space.shape[0]
action_dim = env.action_space.n

assert type(state_dim) is int
assert type(action_dim) is int

model = Network(state_dim , action_dim)
target_model = Network(state_dim , action_dim)
if use_cuda:
    model.cuda()
    target_model.cuda()


memory = ReplayMemory(MEMORY_SIZE)
optimizer = optim.Adam(model.parameters(), LR)
steps_done = 0
episode_durations = []
episode_rewards = []


def update_target_model(model,target_model):
    target_model.load_state_dict(copy.deepcopy(model.state_dict()))


def select_action(state):
    global steps_done
    sample = random.random()
    eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
    steps_done += 1
    if sample > eps_threshold:
        return model(Variable(state, volatile=True).type(FloatTensor)).data.max(1)[1].view(1, 1)
    else:
        return LongTensor([[random.randrange(2)]])


def run_episode(e, environment,fp):
    state = environment.reset()
    steps = 0
    episode_rewards.append(0)
    while True:

        action = select_action(FloatTensor([state]))
        next_state, reward, done, _ = environment.step(action.cpu().numpy()[0,0])
        done  = 1 if done else 0
        # negative reward when attempt ends
        if  next_state[0]>=0.5: 
            reward = 1

        episode_rewards[-1]+=reward

        memory.push((FloatTensor([state]),
                     action,  # action is already a tensor
                     FloatTensor([next_state]),
                     FloatTensor([reward]) ,
                     FloatTensor([done])))

        learn()
        #print(reward,state)

        state = next_state
        steps += 1

        if done:
            print("{2} Episode {0} finished after {1} steps with return {3}"
                  .format(e, steps, '\033[92m' if steps >= 195 else '\033[99m' , episode_rewards[-1]))
            # episode_durations.append(steps)
            # plot_durations()
            #plot_rewards()
            fp.write(','.join([str(e) , str(steps) , str(episode_rewards[-1])]))
            fp.write('\n')
            break


def learn():
    global steps_done

    if steps_done % utm == 0:
        update_target_model(model , target_model)


    if len(memory) < BATCH_SIZE:
        return

    # random transition batch is taken from experience replay memory
    transitions = memory.sample(BATCH_SIZE)
    batch_state, batch_action, batch_next_state, batch_reward,batch_done = zip(*transitions)

    batch_state = Variable(torch.cat(batch_state))
    batch_action = Variable(torch.cat(batch_action))
    batch_reward = Variable(torch.cat(batch_reward))
    batch_next_state = Variable(torch.cat(batch_next_state))
    batch_done = Variable(torch.cat(batch_done))

    # current Q values are estimated by NN for all actions
    current_q_values = model(batch_state).gather(1, batch_action)
    # expected Q values are estimated from actions which gives maximum Q value
    max_next_q_values = target_model(batch_next_state).detach().max(1)[0]
    expected_q_values = batch_reward + (GAMMA * max_next_q_values) *(1- batch_done)
    expected_q_values = expected_q_values.view(-1,1)

    # loss is measured from error between current and newly expected Q values
    #loss = F.smooth_l1_loss(current_q_values, expected_q_values)
    Loss = torch.nn.MSELoss()
    loss = Loss(current_q_values, expected_q_values)

    # backpropagation of loss to NN
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


 


with open('%s_%s.log'%(env_name,suff),'w+') as fp:
    fp.write(','.join(['episode' , 'steps' , 'return']))
    update_target_model(model,target_model)
    for e in range(EPISODES):
        run_episode(e, env,fp)


    print('Complete')

    env.close()
 
