import argparse

import torch


def get_args():
    parser = argparse.ArgumentParser(description='simple dqn task using learning from peer')

    parser.add_argument('-e' , '--env_name' , type=str,default = 'CartPole-v0')

    parser.add_argument('-id' , '--figure_id' , type=int,default = 0)

    parser.add_argument('-s', '--suff' , type=str , default='0',
                        help='log file suff')

    parser.add_argument('-an', '--agent_number', type=int, default=3,
                        help='number of agent')

    parser.add_argument('-we', '--warm_eps', type=int, default=40,
                        help='number of agent')

    parser.add_argument('-ws', '--warm_steps', type=int, default=1000,
                        help='number of agent')


    parser.add_argument('-ee', '--eval_episodes', type=int, default=10,
                        help='number of evaluation episodes')

    parser.add_argument('-te', '--test_episodes', type=int, default=10,
                        help='number of test episodes')

    parser.add_argument('-ep', '--episodes', type=int, default=300,
                        help=' number of episodes')

    parser.add_argument('-ts', '--total_steps', type=int, default=50000,
                        help=' number of total training steps')

    parser.add_argument('-utm', '--update_target_model_freq', type=int, default=50,
                        help='update target model freq/ steps')

    parser.add_argument('-etm', '--eval_target_model_freq', type=int, default=50,
                        help='evaluation target model freq/ steps')

    parser.add_argument('-bc', '--beta_cwa', type=float, default=0.5,
                        help=' coefficient of std when calculate win agent')

    parser.add_argument('-wtc', '--wait_time_cwa', type=float, default=5,
                        help=' max wait time befor change the win agnet when calculate win agent')

    parser.add_argument('-es', '--eps_start', type=int, default=1,
                        help=' e-greedy threshold start value')

    parser.add_argument('-en', '--eps_end', type=int, default=0.01,
                        help=' e-greedy threshold end value')

    parser.add_argument('-ed', '--eps_decay', type=int, default=100,
                        help=' e-greedy threshold decay')

    parser.add_argument('-ga', '--gamma', type=float, default=0.99,
                        help=' Q-learning discount factor')

    parser.add_argument('-lr', '--lr', type=float, default=0.001,
                        help=' NN optimizer learning rate')

    parser.add_argument('-h1', '--hidden_layer1', type=int, default=32,
                        help=' NN hidden layer size')

    parser.add_argument('-h2', '--hidden_layer2', type=int, default=32,
                        help=' NN hidden layer size')

    parser.add_argument('-b', '--batch_size', type=int, default=64,
                        help=' batch_size')

    parser.add_argument('-m', '--memory_size', type=int, default=10000,
                        help=' replay buffer size')

    parser.add_argument('-li', '--log_interval', type=int, default=1,
                        help=' log interval /episode')

    parser.add_argument('--seed', type=int, default=0,
                        help='random seed (default: 0)')

    parser.add_argument('--log-dir', default='/tmp/learning_from_peer/',
                        help='directory to save agent logs (default: /tmp/learning_from_peer/)')

    parser.add_argument('--save-dir', default='./trained_models/',
                        help='directory to save agent logs (default: ./trained_models/)')

    # parser.add_argument('--cuda', type=int, default=-1,
    #                     help='  CUDA index ')

    parser.add_argument('--cuda', nargs='+',
                        help='  CUDA index ')

    parser.add_argument('--use_cuda', action='store_true',
                        help=' whether use cuda ')

    args = parser.parse_args()

    # args.cuda = not args.no_cuda and torch.cuda.is_available()

    return args
