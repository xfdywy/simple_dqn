import gym

class SimpleDQNWrapper(gym.Wrapper):
    def __init__(self ,env, name ):
        self.name = name
        super(SimpleDQNWrapper,self).__init__(env)
        self.action_dim = env.action_space.n
        self.state_dim = env.observation_space.shape[0]
    def _step(self,action):
        obs,reward,done,info = self.env.step(action)

        if 'CartPole' in self.name:
            if done :
                reward = -1
        if 'MountainCar':
            if obs[0] >= 0.5:
                reward = 1



        return obs,reward,done,info