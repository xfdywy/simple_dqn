import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import torch


def make_model(args , state_dim , action_dim ,idx):
    model =  Network_single( state_dim,  action_dim, args.hidden_layer1, args.hidden_layer2)
    if args.use_cuda:
        model = model.to(args.cuda[ idx])
    return  model




class Network_single(nn.Module):
    def __init__(self , state_dim , action_dim , HIDDEN_LAYER1, HIDDEN_LAYER2):
        nn.Module.__init__(self)
        self.l1 = nn.Linear(state_dim, HIDDEN_LAYER1)
        self.l2 = nn.Linear(HIDDEN_LAYER1 , HIDDEN_LAYER2)
        self.l3 = nn.Linear(HIDDEN_LAYER2, action_dim)

    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = self.l3(x)
        return x

class Network(nn.Module):
    def __init__(self , state_dim , action_dim, agent_num , HIDDEN_LAYER1 =32, HIDDEN_LAYER2=32):
        nn.Module.__init__(self)
        self.HIDDEN_LAYER1 = HIDDEN_LAYER1
        self.HIDDEN_LAYER2 = HIDDEN_LAYER2
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.agent_num  = agent_num

        self.multi_agent_layer = nn.ModuleList()
        for ii in range(agent_num):
            self.multi_agent_layer.append(self._make_layer())


    def forward(self, x):
        # x = F.relu(self.l1(x))
        # x = F.relu(self.l2(x))
        # x = x.view(x.size(0),-1)
        # print(x.shape)
        multi_agent = []
        for ii in self.multi_agent_layer:
            multi_agent.append(ii(x))
        out = torch.stack(multi_agent , 2)
        # print()
        return out

    def _make_layer(self):

        l1 = nn.Linear(self.state_dim, self.HIDDEN_LAYER1)
        l2 = nn.Linear(self.HIDDEN_LAYER1, self.HIDDEN_LAYER2)
        l3 = nn.Linear(self.HIDDEN_LAYER2, self.action_dim)
        r1 = nn.ReLU()
        r2 = nn.ReLU()

        model = nn.Sequential(
            l1,
            r1,
            l2,
            r2,
            l3
        )

        return(model)
